import React from 'react';
import {LuSearch} from 'react-icons/lu';
import {PiHandWavingBold} from 'react-icons/pi';
import { RiMoneyDollarCircleLine } from 'react-icons/ri';
import StatCard from '../Utils/StatCard';
import { Input, InputGroup, InputLeftElement } from '@chakra-ui/react';
import ProductSellGrid from './ProductSellGrid';
import Chart from 'react-apexcharts'
import { Select } from '@chakra-ui/react';

const Dashboard = ({ sidebarOpen, setSidebarOpen }) => {

    const ChartOptions = {
        chart: {
          type: 'bar'
        },
        series: [{
          data: [{
            x: 'JAN',
            y: 10
          }, {
            x: 'FEB',
            y: 18
          }, {
            x: 'category C',
            y: 13
          },
          {
            x: 'category D',
            y: 13
          },
          {
            x: 'category E',
            y: 13
          },
          {
            x: 'category F',
            y: 13
          },
          {
            x: 'category F',
            y: 13
          },
          {
            x: 'category F',
            y: 13
          }]
        }]
      };


      const DonutChartOptions = {
          
        series: [44, 55, 41, 17, 15],
        options: {
          chart: {
            type: 'donut',
          },
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: 'bottom'
              }
            }
          }]
        },
      
      
      }
      


  return (
    <div>
        <div className='flex justify-between items-center mb-6'>
            <div>
            <div className="flex">
            <button
              className="text-slate-600 mr-3 hover:text-slate-800 lg:hidden"
              aria-controls="sidebar"
              aria-expanded={sidebarOpen}
              onClick={(e) => {
                e.stopPropagation();
                setSidebarOpen(!sidebarOpen);
              }}
            >
              <svg
                className="w-6 h-6 fill-current"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <rect x="4" y="5" width="16" height="2" />
                <rect x="4" y="11" width="16" height="2" />
                <rect x="4" y="17" width="16" height="2" />
              </svg>
            </button>

            <h1 className='text-2xl font-bold text-gray-800'>Hello Ayush <PiHandWavingBold color='orange' className='inline-block' />,</h1>
          </div>
            </div>
            <div>
             <InputGroup bg>
               <InputLeftElement pointerEvents='none'>
                <LuSearch/>
               </InputLeftElement>
               <Input bg type='text' placeholder='Search' />
             </InputGroup>
            </div>
        </div>



        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6 mb-6'>
            <StatCard AvatarBg={'green.200'} AvatarIcon={<RiMoneyDollarCircleLine size={'38px'} color='green' />} FirstLine={'Earning'} SecondLine={'$198K'} Percentage={'37.8%'} PercentCriteria={'UP'}/>
            <StatCard AvatarBg={'green.200'} AvatarIcon={<RiMoneyDollarCircleLine size={'38px'} color='green' />} FirstLine={'Earning'} SecondLine={'$198K'} Percentage={'37.8%'} PercentCriteria={'UP'}/>
            <StatCard AvatarBg={'green.200'} AvatarIcon={<RiMoneyDollarCircleLine size={'38px'} color='green' />} FirstLine={'Earning'} SecondLine={'$198K'} Percentage={'37.8%'} PercentCriteria={'UP'}/>
            <StatCard AvatarBg={'green.200'} AvatarIcon={<RiMoneyDollarCircleLine size={'38px'} color='green' />} FirstLine={'Earning'} SecondLine={'$198K'} Percentage={'37.8%'} PercentCriteria={'UP'}/>
        </div>

        <div className='flex flex-wrap md:flex-nowrap gap-6 mb-4'>
        <div className='bg-white w-full md:w-8/12 rounded-xl p-4 '>
            <div className='flex justify-between mb-4'>
             <div className='space-y-2'>
                 <h3 className='text-2xl font-bold'>Overview</h3>
                 <p className='text-sm text-gray-400'>Monthly Earning</p>
             </div>
             <div>
             <Select defaultValue={'option1'} placeholder='Select Period'>
               <option  value='option1'>Quarterly</option>
               <option value='option2'>Annually</option>
               <option value='option2'>Monthly</option>
             </Select>
             </div>
            </div>
        <Chart options={ChartOptions} series={ChartOptions.series} type="bar" height={350} />
        </div>
        <div className=' w-full md:w-4/12 bg-white rounded-xl p-4'>
        <Chart options={DonutChartOptions} series={DonutChartOptions.series} type="donut" height={350} />

        </div>
        </div>

        <div >
            <ProductSellGrid/>
        </div>
    </div>
  )
}

export default Dashboard