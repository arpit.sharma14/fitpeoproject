import React from 'react'
import { Input, InputGroup, InputLeftElement } from '@chakra-ui/react';
import {LuSearch} from 'react-icons/lu';
import { Select } from '@chakra-ui/react';

const ProductSellGrid = () => {
  return (
    <div>
        
<div class="relative overflow-x-auto shadow-md rounded-2xl">
    <div class="flex items-center justify-between py-4 px-4 bg-white ">
        <div>
            <h2 className='text-2xl font-bold'>Product Sell</h2>
        </div>

        <div className='flex gap-4'>
        <div>
             <InputGroup bg>
               <InputLeftElement pointerEvents='none'>
                <LuSearch/>
               </InputLeftElement>
               <Input bg type='text' placeholder='Search' />
             </InputGroup>
            </div>

            <div>
             <Select  defaultValue={'option1'} placeholder='Select Period'>
               <option  value='option1'>Last 30 days</option>
               <option value='option2'>Last 7 days</option>
             </Select>
            </div>

        </div>
    </div>
    <table class="w-full text-sm text-left text-gray-500 ">
        <thead class="text-xs text-gray-700 uppercase bg-gray-200 ">
            <tr>
                <th scope="col" class="px-6 py-3">
                    Product Name
                </th>
                <th scope="col" class="px-6 py-3">
                    Stock
                </th>
                <th scope="col" class="px-6 py-3">
                    Price
                </th>
                <th scope="col" class="px-6 py-3">
                    Total Sales
                </th>
            </tr>
        </thead>
        <tbody>
            <tr class="bg-white border-b  hover:bg-gray-50 ">
                <th scope="row" class="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap ">
                    <img class="w-10 h-10 rounded-lg" src="https://img.freepik.com/free-photo/abstract-paint-background-with-multi-colored-watercolor-painting-generated-by-ai_188544-15558.jpg?size=626&ext=jpg&ga=GA1.1.1413502914.1697155200&semt=sph" alt="Jese image"/>
                    <div class="pl-3">
                        <div class="text-base font-bold">Abstract 3D</div>
                        <div class="font-normal text-gray-400 text-sm">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</div>
                    </div>  
                </th> 
                <td class="px-6 py-4 font-bold text-gray-900">
                    32 in Stock
                </td>
                <td class="px-6 py-4 font-bold text-gray-900">
                    $45.99
                </td>
                <td class="px-6 py-4 font-bold text-gray-900">
                    20
                </td>
            </tr>

        </tbody>
    </table>

</div>

    </div>
  )
}

export default ProductSellGrid